# Socrates France Orgnazier

## Prerequire

Retrieve the jar from the jenkins : 
    [artefact link](http://jenkins-lacombe.westeurope.cloudapp.azure.com/jenkins/job/socratesfr/)

## Usage

To run the organizer you'll need [java 8](https://www.java.com/fr/download/) installed and type this command on a terminal : 

```shell
java -jar bestsocrates.jar args...
```

### Feature : Draw and send emails

This feature helps you to select candidates for the event. 
This also choose the best choice for each candidate.

```shell
java -jar bestsocrates.jar DRAW InputFile SingleRooms DoubleRooms TripleRooms NoAccomodation OutputFile
```

* **DRAW** : Command for the draw feature
* **InputFile** : Input file with all candidates (CSV)
* **SingleRooms** : The single rooms capacity : Number of single room available
* **DoubleRooms** : The double rooms capacity : Number of double rooms available * 2
* **TripleRooms** : The triple rooms capacity : Number of triples rooms available * 3
* **NoAccommodation** : The number of available seats without room reservation
* **OuputFile** : Export in a csv file all winner candidates

#### Example
```shell
    java -jar bestsocrates.jar DRAW C:/data/candidate-data.csv 15 20 30 26 C:/src/data/export_socrates_attendee.csv
```

### Feature : Organize Taxi

This feature helps you to organize taxis with the lowest cost for each attendee.
 

```shell
java -jar bestsocrates.jar TAXI InputFile 
```

* **TAXI** : Command for the taxi feature
* **InputFile** : Input file with attendee (CSV)

#### Example 

```shell
java -jar bestsocrates.jar TAXI C:/data/attendee-by-time-arriaval.csv
```


---
*Please reports any bugs to DameRubyAndTheChevalierAuGreenLion.*