import draw.Draw;
import draw.DrawAttendee;
import draw.RoomCapacity;
import export.CsvDrawResultExport;
import export.DrawResultExport;
import notification.*;
import provider.CandidateProvider;
import provider.ChoicesFilter;
import provider.CsvCandidateProvider;
import transport.domain.TaxiOrganizer;
import transport.domain.passenger.PassengerProvider;
import transport.domain.taxi.TaxiFleetPrinter;
import transport.infrastructure.CSVPassengerProvider;
import transport.infrastructure.ConsoleTaxiFleetPrinter;

import java.io.IOException;

public class SocratesFrApplication {

    public static void main(String[] args) throws IOException {

        if (args.length < 2) {
            usage();
        }

        switch (args[0]) {
            case "DRAW":
                Draw(args);
                break;
            case "TAXI":
                Taxi(args);
                break;
        }
    }

    private static void usage() {
        System.err.println("Not enough operand");
        System.out.println("DRAW filePath singleRoomCapacity doubleRoomCapacity tripleRoomCapacity noAccommodationCapacity");
        System.out.println("TAXI filePath");
        System.exit(255);
    }

    private static void Taxi(String... args) {

        String filePath = args[1];
        TaxiFleetPrinter console = new ConsoleTaxiFleetPrinter(System.out::println);
        PassengerProvider passengerProvider = new CSVPassengerProvider(filePath);

        TaxiOrganizer taxiOrganizer = new TaxiOrganizer(passengerProvider, console);

        taxiOrganizer.organize();

    }

    private static void Draw(String... args) {

        if (args.length < 6) {
            usage();
        }

        String filePah = args[1];
        int singleRoomCapacity = Integer.parseInt(args[2]);
        int doubleRoomCapacity = Integer.parseInt(args[3]);
        int tripleRoomCapacity = Integer.parseInt(args[4]);
        int noAccommodationCapacity = Integer.parseInt(args[5]);

        CandidateProvider candidateProvider = new CsvCandidateProvider(filePah, new ChoicesFilter());
        RoomCapacity roomCapacity = new RoomCapacity(singleRoomCapacity, doubleRoomCapacity, tripleRoomCapacity, noAccommodationCapacity);
        Draw drawer = new DrawAttendee(roomCapacity);
        Sender<Email> sender = new EmailSender();
        Notifier<Email> successNotifier = new Notifier<>(new EmailSuccessGenerator(), sender);
        Notifier<Email> failNotifier = new Notifier<>(new EmailFailureGenerator(), sender);
        DrawResultExport drawResultExport = new CsvDrawResultExport(args[6]);

        SummitOrganizer summitOrganizer = new SummitOrganizer(candidateProvider, drawer, successNotifier, failNotifier, drawResultExport);

        summitOrganizer.organize();
    }


}
