import draw.*;
import exeption.DrawResultExportException;
import export.DrawResultExport;
import export.ExportAttendee;
import export.ExportAttendees;
import notification.Destinary;
import notification.Email;
import notification.Notifier;
import provider.CandidateProvider;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SummitOrganizer {
    private final Notifier<Email> successNotifier;
    private final Notifier<Email> failureNotifier;
    private final DrawResultExport drawResultExport;
    private final Draw drawer;
    private final CandidateProvider provider;


    public SummitOrganizer(CandidateProvider provider, Draw drawer, Notifier<Email> successNotifier, Notifier<Email> failureNotifier, DrawResultExport drawResultExport) {

        this.provider = provider;
        this.drawer = drawer;
        this.successNotifier = successNotifier;
        this.failureNotifier = failureNotifier;
        this.drawResultExport = drawResultExport;
    }

    public void organize() {
        Candidates candidates = provider.getCandidates();

        DrawResult drawResult = drawer.draw(candidates);

        for (Attendee attendee : drawResult.getAttendees()) {
            successNotifier.notify(new Destinary(attendee.getEmailAddress(), attendee.getAssignedRoom()));
        }

        for (Candidate candidate : drawResult.getWaitingCandidates()) {
            failureNotifier.notify(new Destinary(candidate.getEmailAddress()));
        }

        try {
            drawResultExport.export(getExportAttendees(drawResult.getAttendees()));
        } catch (DrawResultExportException e) {
            System.err.println("Erreur lors de l'export en csv");
        }
    }

    private ExportAttendees getExportAttendees(List<Attendee> attendees) {
        List<ExportAttendee> exportAttendees = attendees.stream().map(new Function<Attendee, ExportAttendee>() {
            @Override
            public ExportAttendee apply(Attendee attendee) {
                return new ExportAttendee(attendee.getEmailAddress(), attendee.getAssignedRoom().toString());
            }
        }).collect(Collectors.toList());

        ExportAttendees finalExportAttendees = new ExportAttendees();
        exportAttendees.forEach(finalExportAttendees::addAttendee);

        return finalExportAttendees;
    }

}
