package draw;

public class Attendee {
    private final RoomSize assignedRoom;
    private final String emailAddress;

    public Attendee(RoomSize roomSize, String emailAddress) {
        this.assignedRoom = roomSize;
        this.emailAddress = emailAddress;
    }

    boolean isAssignedRoom(RoomSize roomSize) {
        return getAssignedRoom() == roomSize;
    }

    public RoomSize getAssignedRoom() {
        return assignedRoom;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
}
