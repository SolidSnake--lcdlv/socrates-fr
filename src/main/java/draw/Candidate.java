package draw;

import java.util.Objects;

public class Candidate {

    private final CandidateChoices candidateChoices = new CandidateChoices();
    private final String emailAddress;

    public Candidate(String emailAddress, RoomSize... roomChoices) {
        this.emailAddress = emailAddress;
        this.candidateChoices.addAllChoices(roomChoices);
    }

    public RoomSize getChoiceBy(Choice type) {
        return candidateChoices.getChoiceBy(type);
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candidate candidate = (Candidate) o;
        return Objects.equals(emailAddress, candidate.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emailAddress);
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "candidateChoices=" + candidateChoices +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
