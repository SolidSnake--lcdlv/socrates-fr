package draw;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

class CandidateChoices {
    private final Map<Choice, RoomSize> choices = new HashMap<>();

    public CandidateChoices() {
        initChoices();
    }

    private void initChoices() {
        for (Choice choice : Choice.values()) {
            choices.put(choice, RoomSize.UNDEFINED);
        }
    }

    public void addAllChoices(RoomSize[] roomChoices) {

        assert roomChoices.length <= Choice.values().length : "Problem size roomChoices !";

        for (int i = 0; i < roomChoices.length; i++) {
            RoomSize roomChoice = roomChoices[i];
            choices.put(Choice.values()[i], roomChoice);
        }
    }

    public RoomSize getChoiceBy(Choice type) {

        RoomSize roomSize = choices.get(type);

        return roomSize == null ? RoomSize.UNDEFINED : roomSize;
    }

    @Override
    public String toString() {
        return "CandidateChoices{" +
                "choices=" + choices +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CandidateChoices that = (CandidateChoices) o;
        return Objects.equals(choices, that.choices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(choices);
    }

}