package draw;


import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Candidates {

    private final List<Candidate> candidates;

    public Candidates(List<Candidate> candidates) {
        this.candidates = new LinkedList<>(candidates);
    }

    public Candidates() {
        this.candidates = new LinkedList<>();
    }

    public void addCandidate(Candidate candidate) {
        this.candidates.add(candidate);
    }

    public boolean notAlreadyExist(Candidate candidate) {
        return !candidates.contains(candidate);
    }

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public Candidate get(int index) {
        return candidates.get(index);
    }

    public Candidates duplicate() {
        return new Candidates(candidates);
    }

    public void shuffle() {
        Collections.shuffle(candidates);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candidates that = (Candidates) o;
        return Objects.equals(candidates, that.candidates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(candidates);
    }

    @Override
    public String toString() {
        return "Candidates{" +
                "candidates=" + candidates +
                '}';
    }
}






