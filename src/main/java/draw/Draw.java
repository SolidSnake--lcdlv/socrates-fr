package draw;

public interface Draw {
    DrawResult draw(Candidates candidatesWithChoice);
}
