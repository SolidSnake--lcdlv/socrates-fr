package draw;

import java.util.Iterator;


public class DrawAttendee implements Draw {

    private final RoomCapacity roomCapacity;

    public DrawAttendee(RoomCapacity roomCapacity) {
        this.roomCapacity = roomCapacity;
    }

    @Override
    public DrawResult draw(Candidates candidates) {
        candidates.shuffle();

        DrawResult drawResult = new DrawResult(candidates);

        for (Choice choice : Choice.values()) {
            assignRoomForEachCandidate(drawResult, choice);
        }

        return drawResult;
    }

    private void assignRoomForEachCandidate(DrawResult drawResult, Choice choice) {
        Iterator<Candidate> iterator = drawResult.iterateOnCandidates();

        while (iterator.hasNext()) {
            assignRoomIfAvailable(drawResult, choice, iterator);
        }
    }

    private void assignRoomIfAvailable(DrawResult drawResult, Choice choice, Iterator<Candidate> iterator) {
        Candidate candidate = iterator.next();
        RoomSize roomSize = candidate.getChoiceBy(choice);

        if (roomCapacity.roomAvailable(roomSize)) {
            drawResult.selectCandidate(candidate, roomSize);
            roomCapacity.decrementRoomCapacity(roomSize);
            iterator.remove();
        }
    }
}
