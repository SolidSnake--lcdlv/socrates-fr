package draw;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DrawResult {
    private final List<Attendee> attendees;
    private final Candidates waitingCandidates;

    public DrawResult(Candidates candidates) {
        attendees = new ArrayList<>();
        waitingCandidates = candidates.duplicate();
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }

    public List<Candidate> getWaitingCandidates() {
        return waitingCandidates.getCandidates();
    }

    public void selectCandidate(Candidate candidate, RoomSize roomSize) {
        Attendee attendee = new Attendee(roomSize, candidate.getEmailAddress());
        attendees.add(attendee);
    }

    public Iterator<Candidate> iterateOnCandidates() {
        return waitingCandidates.getCandidates().iterator();
    }

}
