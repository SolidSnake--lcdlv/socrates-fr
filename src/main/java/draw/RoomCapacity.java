package draw;

import java.util.HashMap;
import java.util.Map;

import static draw.RoomSize.*;

public class RoomCapacity {
    private final Map<RoomSize, Integer> roomsCapacity;

    public RoomCapacity(int singleRoomCapacity, int doubleRoomCapacity, int tripleRoomCapacity, int noAccomodationCapacity) {
        roomsCapacity = new HashMap<>();
        roomsCapacity.put(SINGLE, singleRoomCapacity);
        roomsCapacity.put(DOUBLE, doubleRoomCapacity);
        roomsCapacity.put(TRIPLE, tripleRoomCapacity);
        roomsCapacity.put(NO_ACCOMODATION, noAccomodationCapacity);
        roomsCapacity.put(UNDEFINED, 0);
    }

    boolean roomAvailable(RoomSize roomSize) {
        return roomsCapacity.get(roomSize) > 0;
    }

    void decrementRoomCapacity(RoomSize roomSize) {
        roomsCapacity.put(roomSize, roomsCapacity.get(roomSize) - 1);
    }

}