package draw;

public enum RoomSize {
    SINGLE("single"), DOUBLE("double"), TRIPLE("triple"), NO_ACCOMODATION("no accomodation"), UNDEFINED("undefined");

    private final String description;

    RoomSize(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
