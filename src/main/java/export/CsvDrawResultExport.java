package export;

import exeption.DrawResultExportException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class CsvDrawResultExport implements DrawResultExport {

    private final String HEADER = "EMAIL, ROOM_TYPE";
    private final String filePath;
    private PrintWriter printWriter;

    public CsvDrawResultExport(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void export(ExportAttendees exportAttendees) throws DrawResultExportException {

        if (exportAttendees.isNotPresent()) {
            return;
        }

        try (FileWriter fileWriter = new FileWriter(createNewFileTo(filePath))) {

            printWriter = new PrintWriter(fileWriter);
            printWriter.println(HEADER);
            exportAttendees.print(this::write);

        } catch (IOException e) {
            throw new DrawResultExportException();
        }
    }

    private void write(List<String> lines) {
        lines.forEach(printWriter::println);
    }

    private File createNewFileTo(String filePath) throws IOException {
        File file = new File(filePath);
        file.createNewFile();
        return file;
    }
}
