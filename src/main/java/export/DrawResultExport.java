package export;

import exeption.DrawResultExportException;

public interface DrawResultExport {

    void export(ExportAttendees exportAttendees) throws DrawResultExportException;

}
