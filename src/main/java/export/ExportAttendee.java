package export;

public class ExportAttendee {

    private final String email;
    private final String roomSize;

    public ExportAttendee(String email, String roomSize) {

        this.email = email;
        this.roomSize = roomSize;
    }

    @Override
    public String toString() {
        return email + ", " + roomSize;
    }
}
