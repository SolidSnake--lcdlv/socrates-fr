package export;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ExportAttendees {

    private final List<ExportAttendee> exportAttendees;

    public ExportAttendees() {
        this.exportAttendees = new ArrayList<>();
    }

    public void addAttendee(ExportAttendee exportAttendee) {
        exportAttendees.add(exportAttendee);
    }

    public boolean isNotPresent() {
        return exportAttendees.isEmpty();
    }

    public void print(Consumer<List<String>> printFunction) {
        printFunction.accept(exportAttendees.stream().map(ExportAttendee::toString).collect(Collectors.toList()));
    }
}
