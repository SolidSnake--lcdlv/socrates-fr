package notification;

import draw.RoomSize;

public class Destinary {

    private final RoomSize assignedRoom;
    private final String emailAddress;

    public Destinary(String addressMail) {
        this.emailAddress = addressMail;
        this.assignedRoom = RoomSize.UNDEFINED;
    }

    public Destinary(String emailAddress, RoomSize assignedRoom) {
        this.emailAddress = emailAddress;
        this.assignedRoom = assignedRoom;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public RoomSize getAssignedRoom() {
        return assignedRoom;
    }
}
