package notification;

import java.util.Objects;

public class Email {

    private final Destinary to;
    private final EmailTemplate template;

    public Email(Destinary destinary, EmailTemplate emailTemplate) {
        this.to = destinary;
        this.template = emailTemplate;
    }

    public String getTo() {
        return to.getEmailAddress();
    }

    public String getBody() {
        return template.getBody();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email email = (Email) o;
        return Objects.equals(to, email.to) &&
                Objects.equals(template, email.template);
    }

    @Override
    public int hashCode() {
        return Objects.hash(to, template);
    }

    @Override
    public String toString() {
        return "To : " + getTo() + "\n" + template.toString();
    }
}
