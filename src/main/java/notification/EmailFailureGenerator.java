package notification;

public class EmailFailureGenerator extends EmailGenerator {
    @Override
    protected String getBody(Destinary destinary) {
        return String.format("Sorry %s,\nYou lose!", destinary.getEmailAddress());
    }
}
