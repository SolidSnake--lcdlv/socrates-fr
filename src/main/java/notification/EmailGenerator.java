package notification;

public abstract class EmailGenerator implements Generator<Email> {

    @Override
    public Email generate(Destinary destinary) {
        EmailTemplate template  = new EmailTemplate("contact@socrates.fr", "Socrates 2017!", getBody(destinary));
        return new Email(destinary, template);
    }

    abstract protected String getBody(Destinary destinary);

}
