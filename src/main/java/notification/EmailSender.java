package notification;

public class EmailSender implements Sender<Email> {

    @Override
    public void sendEmail(Email email) {
        System.out.println(email.toString());
    }
}
