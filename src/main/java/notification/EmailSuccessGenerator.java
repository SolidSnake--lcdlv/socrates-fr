package notification;

public class EmailSuccessGenerator extends EmailGenerator {
    @Override
    protected String getBody(Destinary destinary) {
        return String.format("SocratesFrApplication %s\nYou win a %s room.", destinary.getEmailAddress(), destinary.getAssignedRoom());
    }
}
