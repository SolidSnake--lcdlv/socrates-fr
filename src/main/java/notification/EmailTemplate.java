package notification;

import java.util.Objects;

public class EmailTemplate {
    private final String from;
    private final String object;
    private final String body;

    public EmailTemplate(String from, String object, String body) {
        this.from = from;
        this.object = object;
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailTemplate that = (EmailTemplate) o;
        return Objects.equals(from, that.from) &&
                Objects.equals(object, that.object) &&
                Objects.equals(body, that.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, object, body);
    }

    @Override
    public String toString() {
        return "From : " + from + "\n" +
                "Object : " + object + "\n" +
                "-----\n" +
                body + "\n\n" +
                "=============";
    }
}
