package notification;

public interface Generator<T> {

    T generate(Destinary destinary);
}
