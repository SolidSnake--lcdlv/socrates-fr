package notification;

public class Notifier<T> {

    private final Generator<T> generator;
    private final Sender<T> sender;

    public Notifier(Generator<T> generator, Sender<T> sender) {
        this.generator = generator;
        this.sender = sender;
    }

    public void notify(Destinary destinary) {

        T generatedEmail = generator.generate(destinary);

        sender.sendEmail(generatedEmail);

    }
}
