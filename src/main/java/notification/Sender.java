package notification;


public interface Sender<T> {

    void sendEmail(T toSend);

}
