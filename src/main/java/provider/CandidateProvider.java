package provider;

import draw.Candidates;

public interface CandidateProvider {

    Candidates getCandidates();
}
