package provider;

import draw.RoomSize;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class ChoicesFilter {

    RoomSize[] removeDuplicateRoomSize(RoomSize[] roomChoices) {
        LinkedHashSet<RoomSize> link = new LinkedHashSet<>();
        link.addAll(Arrays.asList(roomChoices));
        return link.toArray(new RoomSize[0]);
    }
}