package provider;

import draw.Candidate;
import draw.Candidates;
import draw.RoomSize;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import static draw.RoomSize.NO_ACCOMODATION;
import static draw.RoomSize.UNDEFINED;

public class CsvCandidateProvider implements CandidateProvider {

    private static final int EMAIL = 0;
    private static final int FIRST_CHOICE = 1;
    private static final int SECOND_CHOICE = 2;
    private static final int THIRD_CHOICE = 3;
    private static final int FOURTH_CHOICE = 4;

    private final String candidatesFilePath;
    private final ChoicesFilter choicesFilter;

    public CsvCandidateProvider(String candidatesFilePath, ChoicesFilter choicesFilter) {
        this.candidatesFilePath = candidatesFilePath;
        this.choicesFilter = choicesFilter;
    }

    @Override
    public Candidates getCandidates() {
        try {
            return getCandidatesFromCsv(candidatesFilePath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private List<String> readFile(String path) throws IOException {
        Path filePath = Paths.get(path);

        return Files.readAllLines(filePath);
    }

    private Candidates getCandidatesFromCsv(String candidatesFilePath) throws IOException {
        return this.getCandidatesFromLines(this.readFile(candidatesFilePath));
    }

    private Candidates getCandidatesFromLines(List<String> lines) {

        Candidates candidates = new Candidates();
        List<String> linesMinusHeader = lines.subList(1, lines.size());
        for (String line : linesMinusHeader) {

            String[] lineSplitted = explodeCsv(line);

            if (isValidLine(lineSplitted)) {
                Candidate candidate = getCandidateFromColumn(lineSplitted);
                if (candidates.notAlreadyExist(candidate)) {
                    candidates.addCandidate(candidate);
                }
            }
        }
        return candidates;
    }

    private RoomSize parseRoomSize(String choice) {

        if (Objects.isNull(choice) || "".equals(choice)) {
            return UNDEFINED;
        }

        if ("No Accomodation".equals(choice)) {
            return NO_ACCOMODATION;
        }
        return RoomSize.valueOf(choice.toUpperCase());
    }

    private String getColumn(String[] columns, int index) {
        return index < columns.length ? columns[index] : "";
    }

    private String[] explodeCsv(String line) {
        return line.split(",");
    }

    private boolean isValidLine(String[] lineSplitted) {
        return lineSplitted.length > 0 && !lineSplitted[EMAIL].isEmpty();
    }

    private Candidate getCandidateFromColumn(String[] columns) {
        String emailAddress = getColumn(columns, EMAIL);
        RoomSize firstChoice = parseRoomSize(getColumn(columns, FIRST_CHOICE));
        RoomSize secondChoice = parseRoomSize(getColumn(columns, SECOND_CHOICE));
        RoomSize thirdChoice = parseRoomSize(getColumn(columns, THIRD_CHOICE));
        RoomSize fourthChoice = parseRoomSize(getColumn(columns, FOURTH_CHOICE));

        RoomSize[] fiterRoomSize = choicesFilter.removeDuplicateRoomSize(new RoomSize[]{firstChoice, secondChoice, thirdChoice, fourthChoice});
        return new Candidate(emailAddress, fiterRoomSize);

    }
}
