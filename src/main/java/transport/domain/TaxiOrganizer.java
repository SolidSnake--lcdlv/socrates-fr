package transport.domain;

import transport.domain.passenger.PassengerProvider;
import transport.domain.passenger.Passengers;
import transport.domain.taxi.TaxiFleet;
import transport.domain.taxi.TaxiFleetPrinter;

public class TaxiOrganizer {

    private final PassengerProvider passengerProvider;
    private final TaxiFleetPrinter consolePrinter;

    public TaxiOrganizer(PassengerProvider passengerProvider, TaxiFleetPrinter consolePrinter) {
        this.passengerProvider = passengerProvider;
        this.consolePrinter = consolePrinter;
    }

    public void organize() {
        Passengers passengers = passengerProvider.gatherPassengers();

        TaxiFleet taxiFleet = TaxiFleet.divideShipment(passengers.groupByMoment());

        consolePrinter.printAssignments(taxiFleet);

    }
}
