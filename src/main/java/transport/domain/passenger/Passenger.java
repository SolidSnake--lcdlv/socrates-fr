package transport.domain.passenger;

import transport.domain.taxi.Moment;

import java.util.Objects;

public class Passenger {

    private final String name;
    private final Moment arrivaltime;

    public Passenger(String name, Moment arrivalTime) {
        this.name = name;
        this.arrivaltime = arrivalTime;
    }

    public Moment getArrivaltime() {
        return arrivaltime;
    }

    public String getFormattedName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passenger passenger = (Passenger) o;
        return Objects.equals(name, passenger.name) &&
                Objects.equals(arrivaltime, passenger.arrivaltime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, arrivaltime);
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "generateTaxiFleet='" + name + '\'' +
                ", arrivaltime=" + arrivaltime +
                '}';
    }

}