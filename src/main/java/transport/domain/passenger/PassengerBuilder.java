package transport.domain.passenger;

import transport.domain.taxi.Moment;

public final class PassengerBuilder {

    private String name;
    private Moment moment;

    private PassengerBuilder() {
    }

    public static PassengerBuilder aPassenger() {
        return new PassengerBuilder();
    }

    public PassengerBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PassengerBuilder withMoment(Moment moment) {
        this.moment = moment;
        return this;
    }

    public Passenger build() {
        return new Passenger(name, moment);
    }
}
