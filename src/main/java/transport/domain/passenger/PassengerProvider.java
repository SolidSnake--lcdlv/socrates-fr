package transport.domain.passenger;

public interface PassengerProvider {
    Passengers gatherPassengers();

}
