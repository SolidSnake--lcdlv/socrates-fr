package transport.domain.passenger;

public class PassengerProviderException extends RuntimeException {

    public PassengerProviderException(String message) {
        super(message);
    }
}
