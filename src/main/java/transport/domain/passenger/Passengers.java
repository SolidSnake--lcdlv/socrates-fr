package transport.domain.passenger;

import transport.domain.taxi.Shipment;
import transport.domain.taxi.Shipments;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class Passengers {
    private final List<Passenger> passengers;

    public Passengers(List<Passenger> passengers) {
        this.passengers = new ArrayList<>(passengers);
    }

    public Passengers() {
        passengers = new ArrayList<>();
    }

    public List<Passenger> getPassengers() {
        return new ArrayList<>(passengers);
    }

    public Passengers addPassenger(Passenger passenger) {
        passengers.add(passenger);
        return this;
    }

    public Shipments groupByMoment() {
        return Shipments.optimize(passengers.stream()
                .collect(groupingBy(Passenger::getArrivaltime))
                .entrySet()
                .stream()
                .map(entry -> new Shipment(entry.getKey(), new Passengers(entry.getValue()))
                ).collect(toList()));
    }

    public Passengers extract(int extractNumber) {
        int min = Math.min(extractNumber, passengers.size());
        return new Passengers(passengers.subList(0, min));
    }

    public Passengers minus(Passengers passengersToRemove) {
        List<Passenger> passengers = new ArrayList<>(this.passengers);
        passengers.removeAll(passengersToRemove.passengers);
        return new Passengers(passengers);
    }

    @Override
    public String toString() {
        return "Passengers{" +
                "\npassengers=" + passengers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passengers that = (Passengers) o;
        return Objects.equals(passengers, that.passengers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(passengers);
    }
}