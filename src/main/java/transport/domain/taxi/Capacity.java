package transport.domain.taxi;

import java.util.Objects;

public class Capacity {

    private final int value;

    public Capacity(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Capacity capacity = (Capacity) o;
        return value == capacity.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
