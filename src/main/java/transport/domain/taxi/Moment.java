package transport.domain.taxi;

import java.util.Objects;

public class Moment {

    private final String moment;

    public Moment(String moment) {
        this.moment = moment;
    }

    @Override
    public String toString() {
        return "Moment{" +
                "moment='" + moment + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Moment moment1 = (Moment) o;
        return Objects.equals(moment, moment1.moment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(moment);
    }

    public String format() {
        return moment;
    }
}
