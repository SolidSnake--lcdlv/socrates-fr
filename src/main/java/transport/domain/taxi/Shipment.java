package transport.domain.taxi;

import transport.domain.passenger.Passenger;
import transport.domain.passenger.Passengers;

import java.util.Objects;

public class Shipment {

    private final Moment moment;
    private Passengers passengers;

    public Shipment(Moment moment, Passengers passengers) {
        this.moment = moment;
        this.passengers = passengers;
    }

    public Shipment extract(int extractNumber) {
        Passengers passengersExtracted = this.passengers.extract(extractNumber);
        passengers = passengers.minus(passengersExtracted);

        return new Shipment(moment, passengersExtracted);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shipment shipment = (Shipment) o;
        return Objects.equals(moment, shipment.moment) &&
                Objects.equals(passengers, shipment.passengers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(moment, passengers);
    }

    @Override
    public String toString() {
        return "Shipment{" +
                "\nmoment=" + moment +
                ", \npassengers=" + passengers +
                '}';
    }

    public String formatMoment() {
        return moment.format();
    }

    String formatPassengers() {
        StringBuilder formatPassengers = new StringBuilder();
        String separator = "";
        for (Passenger passenger : passengers.getPassengers()) {
            formatPassengers
                    .append(separator)
                    .append(passenger.getFormattedName());
            separator = " | ";
        }
        return formatPassengers.toString();
    }

    public int size() {
        return passengers.getPassengers().size();
    }

}
