package transport.domain.taxi;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Shipments {

    private List<Shipment> shipments;

    private Shipments(List<Shipment> shipments) {
        this.shipments = shipments;
    }

    private Shipments() {
    }

    public static Shipments optimize(List<Shipment> shipments) {
        List<Shipment> splitedShipments = new ArrayList<>();

        for (Shipment shipment : shipments) {
            splitedShipments.addAll(new Shipments().optimizeSplit(shipment));
        }

        return new Shipments(splitedShipments);
    }

    public List<Taxi> forEachShipment(Function<Shipment, Taxi> function) {
        return shipments.stream().map(function).collect(Collectors.toList());
    }

    private List<Shipment> optimizeSplit(Shipment shipment) {
        return optimizeRepartition(shipment).stream().map(shipment::extract).collect(Collectors.toList());
    }

    private List<Integer> optimizeRepartition(Shipment shipment) {
        int size = shipment.size();
        boolean canMakeGroupOfSix = true;
        List<Integer> groupsSize = new ArrayList<>();
        while (size > 0) {
            int groupSize = numberOfPassengers(size, canMakeGroupOfSix);
            size -= groupSize;
            if (groupSize == 6) {
                canMakeGroupOfSix = false;
            }
            groupsSize.add(groupSize);
        }
        return groupsSize;
    }

    private int numberOfPassengers(int size, boolean canMakeGroupOfSix) {
        if (size <= 4) return size;
        if (size == 8) return 4;
        if (size == 7) return 4;
        if (size == 5) return 3;
        if (size == 6 && !canMakeGroupOfSix) return 3;
        if (size >= 6 && canMakeGroupOfSix) return 6;
        if (size == 9) return 3;
        return 4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shipments shipments1 = (Shipments) o;
        return Objects.equals(shipments, shipments1.shipments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shipments);
    }

    @Override
    public String toString() {
        return "Shipments{" +
                "\nshipments=" + shipments +
                '}';
    }
}
