package transport.domain.taxi;

import java.util.Objects;

public class Taxi {
    private final Capacity capacity;
    private final Shipment shipment;

    public Taxi(Shipment shipment, Capacity capacity) {
        this.capacity = capacity;
        this.shipment = shipment;
    }

    public String format() {
        return String.format("%s seats | %s | %s", capacity, shipment.formatMoment(), shipment.formatPassengers());
    }

    @Override
    public String toString() {
        return "Taxi{" +
                "\ncapacity=" + capacity +
                ", \nshipment=" + shipment +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Taxi taxi = (Taxi) o;
        return Objects.equals(capacity, taxi.capacity) &&
                Objects.equals(shipment, taxi.shipment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity, shipment);
    }

}