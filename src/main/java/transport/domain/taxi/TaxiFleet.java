package transport.domain.taxi;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public class TaxiFleet {

    private final List<Taxi> taxiFleet;

    public TaxiFleet() {
        this.taxiFleet = new ArrayList<>();
    }

    private TaxiFleet(List<Taxi> taxis) {
        this.taxiFleet = taxis;
    }

    public static TaxiFleet divideShipment(Shipments shipments) {
        List<Taxi> taxis = shipments.forEachShipment(shipment -> new Taxi(shipment, new Capacity(shipment.size() > 4 ? 6 : 4)));
        return new TaxiFleet(taxis);
    }

    public void addTaxi(Taxi taxi) {
        taxiFleet.add(taxi);
    }

    public void print(Consumer<String> consumer) {
        taxiFleet.forEach(taxi -> consumer.accept(taxi.format()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaxiFleet taxiFleet1 = (TaxiFleet) o;
        return Objects.equals(taxiFleet, taxiFleet1.taxiFleet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taxiFleet);
    }

    @Override
    public String toString() {
        return "TaxiFleet{" +
                "\ntaxiFleet=" + taxiFleet +
                '}';
    }
}
