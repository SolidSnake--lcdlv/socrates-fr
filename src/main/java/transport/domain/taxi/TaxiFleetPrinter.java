package transport.domain.taxi;

public interface TaxiFleetPrinter {
    void printAssignments(TaxiFleet taxiFleet);
}
