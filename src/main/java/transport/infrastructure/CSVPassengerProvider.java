package transport.infrastructure;

import transport.domain.passenger.Passenger;
import transport.domain.passenger.PassengerProvider;
import transport.domain.passenger.PassengerProviderException;
import transport.domain.passenger.Passengers;
import transport.domain.taxi.Moment;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static transport.domain.passenger.PassengerBuilder.aPassenger;

public class CSVPassengerProvider implements PassengerProvider {

    private final String path;

    public CSVPassengerProvider(String path) {
        this.path = path;
    }

    @Override
    public Passengers gatherPassengers() {
        Path filePath = Paths.get(path);
        List<String> lines;

        try {
            lines = Files.readAllLines(filePath);
        } catch (IOException e) {
            throw new PassengerProviderException("Unable to read the file (recorded path: " + path);
        }

        return parseAllLines(lines.subList(1, lines.size()));
    }

    public Passenger parse(String validLine) {
        String[] columns = validLine.split(",");

        return aPassenger().withName(extractName(columns[0]))
                .withMoment(extractTime(columns[2]))
                .build();
    }

    public Passengers parseAllLines(List<String> lines) {
        return new Passengers(lines.stream()
                .map(this::parse)
                .collect(Collectors.toList()));
    }

    private Moment extractTime(String moment) {
        return new Moment(moment);
    }

    private String extractName(String nameWithTwitter) {
        String[] elements = nameWithTwitter.split("@");
        String name = elements[0];
        return name.trim();
    }
}