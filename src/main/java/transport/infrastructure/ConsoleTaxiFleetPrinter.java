package transport.infrastructure;

import transport.domain.taxi.TaxiFleet;
import transport.domain.taxi.TaxiFleetPrinter;

import java.util.function.Consumer;

public class ConsoleTaxiFleetPrinter implements TaxiFleetPrinter {

    private final Consumer<String> consumer;

    public ConsoleTaxiFleetPrinter(Consumer<String> consumer) {
        this.consumer = consumer;
    }

    public static ConsoleTaxiFleetPrinter Build(Consumer<String> printer){
        return new ConsoleTaxiFleetPrinter(printer);
    }

    @Override
    public void printAssignments(TaxiFleet taxiFleet) {
        consumer.accept("Taxi Type | Date | Passenger #1 | Passenger #2 | Passenger #3 | Passenger #4 | Passenger #5 | Passenger #6");

        taxiFleet.print(consumer);
    }

}
