import draw.Draw;
import draw.DrawAttendee;
import draw.RoomCapacity;
import export.CsvDrawResultExport;
import export.DrawResultExport;
import notification.*;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import provider.CandidateProvider;
import provider.ChoicesFilter;
import provider.CsvCandidateProvider;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class SummitOrganizerFeature {

    private static final String YOU_WIN_MESSAGE = "You win!";
    private static final String YOU_WIN_OBJECT = "Socrates 2017!";
    private static final String YOU_LOSE_MESSAGE = "You lose!";
    private static final String YOU_LOSE_OBJECT = "Socrates 2017!";
    private static final String CONTACT_FROM = "contact@socrates.fr";

    @Mock
    Sender sender;

    @Captor
    ArgumentCaptor<Email> emailArgumentCaptor;
    private ChoicesFilter choiceFilter;

    @Before
    public void setUp() throws Exception {
        choiceFilter = new ChoicesFilter();
    }

    @Test
    public void read_file_and_notify_attendee_and_export_winner() throws Exception {
        //given
        String candidatesFilePath = "src/test/resources/candidates-acceptance.csv";

        CandidateProvider provider = new CsvCandidateProvider(candidatesFilePath, choiceFilter);

        RoomCapacity roomCapacity = new RoomCapacity(1, 1, 1, 1);
        Draw drawer = new DrawAttendee(roomCapacity);

        Generator successGenerator = new EmailSuccessGenerator();
        Notifier successNotifier = new Notifier<>(successGenerator, sender);
        Generator failureGenerator = new EmailFailureGenerator();
        Notifier failureNotifier = new Notifier<>(failureGenerator, sender);

        String exportFilePath = "src/test/resources/export_socrates_attendee.csv";
        DrawResultExport drawResultExport = new CsvDrawResultExport(exportFilePath);

        SummitOrganizer summitOrganizer = new SummitOrganizer(provider, drawer, successNotifier, failureNotifier, drawResultExport);

        //when
        summitOrganizer.organize();

        //then
        Set<String> valideMails = new HashSet<>();
        valideMails.add("single_win@lcdlv.fr");
        valideMails.add("double_win@lcdlv.fr");
        valideMails.add("triple_win@lcdlv.fr");
        valideMails.add("single_lost@lcdlv.fr");
        valideMails.add("double_lost@lcdlv.fr");
        valideMails.add("triple_lost@lcdlv.fr");
        valideMails.add("no_accomodation_win@lcdlv.fr");

        verify(sender, times(7)).sendEmail(emailArgumentCaptor.capture());

        List<Email> emails = emailArgumentCaptor.getAllValues();
        assertThat(emails).areExactly(4, emailBodyContains("You win"));
        assertThat(emails).areExactly(3, emailBodyContains("You lose"));
        assertThat(emails.stream().map(email -> email.getTo()).collect(Collectors.toSet())).isEqualTo(valideMails);

        List<String> allLines = Files.readAllLines(Paths.get(exportFilePath));

        assertThat(allLines.size()).isEqualTo(5);
    }

    private Condition<Email> emailBodyContains(final String match) {
        return new Condition<Email>() {
            @Override
            public boolean matches(Email email) {
                return email.toString().contains(match);
            }
        };
    }
}
