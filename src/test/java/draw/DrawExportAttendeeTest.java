package draw;

import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static draw.RoomSize.*;
import static org.assertj.core.api.Assertions.assertThat;

public class DrawExportAttendeeTest {

    private static final int SINGLE_ROOM_CAPACITY = 4;
    private static final int DOUBLE_ROOM_CAPACITY = 6;
    private static final int TRIPLE_ROOM_CAPACITY = 9;

    @Test
    public void no_candidate() {
        Candidates candidatesWithChoice = new Candidates();

        DrawResult result = new DrawAttendee(new RoomCapacity(SINGLE_ROOM_CAPACITY, 0, TRIPLE_ROOM_CAPACITY, 0)).draw(candidatesWithChoice);

        assertThat(result.getAttendees()).isEmpty();
    }

    @Test
    public void two_candidates_with_enough_seats() throws Exception {
        Candidates candidatesWithChoice = new Candidates(listCandidateGenerator(2, new RoomSize[]{SINGLE}));
        DrawResult result = new DrawAttendee(new RoomCapacity(SINGLE_ROOM_CAPACITY, 0, TRIPLE_ROOM_CAPACITY, 0)).draw(candidatesWithChoice);

        assertThat(result.getAttendees()).hasSize(2);
    }

    @Test
    public void more_candidates_than_seats() throws Exception {
        Candidates candidatesWithChoice = new Candidates(listCandidateGenerator(6, new RoomSize[]{SINGLE}));

        DrawResult result = new DrawAttendee(new RoomCapacity(SINGLE_ROOM_CAPACITY, 0, TRIPLE_ROOM_CAPACITY, 0)).draw(candidatesWithChoice);

        assertThat(result.getAttendees()).hasSize(SINGLE_ROOM_CAPACITY);
    }

    @Test
    public void fill_capacity_with_first_choices_single_room() {
        Candidates candidatesWithChoice = new Candidates();
        candidatesWithChoice.addCandidate(new Candidate("", RoomSize.SINGLE));
        candidatesWithChoice.addCandidate(new Candidate("", RoomSize.DOUBLE));

        DrawResult selectedCandidates = new DrawAttendee(new RoomCapacity(SINGLE_ROOM_CAPACITY, 0, TRIPLE_ROOM_CAPACITY, 0)).draw(candidatesWithChoice);

        assertThat(selectedCandidates.getAttendees().stream()
                .filter(attendee -> attendee.isAssignedRoom(SINGLE)))
                .hasSize(1);
    }

    @Test
    public void fill_capacity_with_first_choices_double_room() throws Exception {

        Candidates candidatesWithChoiceDouble = new Candidates(listCandidateGenerator(10, new RoomSize[]{DOUBLE}));
        DrawResult result = new DrawAttendee(new RoomCapacity(0, DOUBLE_ROOM_CAPACITY, TRIPLE_ROOM_CAPACITY, 0)).draw(candidatesWithChoiceDouble);
        assertThat(result.getAttendees())
                .isNotEmpty()
                .allMatch(attendee -> attendee.isAssignedRoom(DOUBLE));
    }

    @Test
    public void fill_capacity_with_first_choices_double_room_and_single_room_with_more_candidates_than_capacity() throws Exception {

        List<Candidate> allCandidate = listCandidateGenerator(10, new RoomSize[]{DOUBLE});
        allCandidate.addAll(listCandidateGenerator(10, new RoomSize[]{SINGLE}));

        DrawResult result = new DrawAttendee(new RoomCapacity(4, 6, 9, 0)).draw(new Candidates(allCandidate));

        assertThat(result.getAttendees().stream()
                .filter(attendee -> attendee.isAssignedRoom(SINGLE)))
                .hasSize(4);
        assertThat(result.getAttendees().stream()
                .filter(attendee -> attendee.isAssignedRoom(DOUBLE)))
                .hasSize(DOUBLE_ROOM_CAPACITY);
    }

    @Test
    public void fill_capacity_with_first_choices_triple_room() throws Exception {

        Candidates candidates = new Candidates(listCandidateGenerator(10, new RoomSize[]{TRIPLE}));
        DrawResult result = new DrawAttendee(new RoomCapacity(SINGLE_ROOM_CAPACITY, DOUBLE_ROOM_CAPACITY, TRIPLE_ROOM_CAPACITY, 0)).draw(candidates);
        assertThat(result.getAttendees().stream().filter(attendee -> attendee.isAssignedRoom(TRIPLE)))
                .hasSize(TRIPLE_ROOM_CAPACITY);
    }

    @Test
    public void using_the_third_choice() throws Exception {
        int singleRoomCapacity = 1;
        int doubleRoomCapacity = 0;
        int tripleRoomCapacity = 1;

        Candidates candidates = new Candidates(listCandidateGenerator(3, new RoomSize[]{SINGLE, DOUBLE, TRIPLE}));

        DrawResult result = new DrawAttendee(new RoomCapacity(singleRoomCapacity, doubleRoomCapacity, tripleRoomCapacity, 0)).draw(candidates);

        assertThat(result.getAttendees().stream().filter(attendee -> attendee.isAssignedRoom(TRIPLE))).hasSize(tripleRoomCapacity);
        assertThat(result.getAttendees().stream().filter(attendee -> attendee.isAssignedRoom(DOUBLE))).hasSize(doubleRoomCapacity);
        assertThat(result.getAttendees().stream().filter(attendee -> attendee.isAssignedRoom(SINGLE))).hasSize(singleRoomCapacity);
    }

    @Test
    public void should_add_unselected_candidate_to_waiting_list() throws Exception {
        Candidates candidates = new Candidates(listCandidateGenerator(3, new RoomSize[]{SINGLE}));

        DrawResult result = new DrawAttendee(new RoomCapacity(1, 0, 0, 0)).draw(candidates);

        assertThat(result.getWaitingCandidates()).hasSize(2);
        assertThat(result.getAttendees()).hasSize(1);
    }

    @Test
    public void should_add_one_candidate_without_accomodation() throws Exception {
        Candidates candidates = new Candidates(listCandidateGenerator(1, new RoomSize[]{NO_ACCOMODATION}));
        DrawResult result = new DrawAttendee(new RoomCapacity(0, 0, 0, 1)).draw(candidates);
        assertThat(result.getAttendees()).hasSize(1);
        assertThat(result.getAttendees().get(0).getAssignedRoom()).isEqualTo(NO_ACCOMODATION);
    }

    private List<Candidate> listCandidateGenerator(int quantity, RoomSize[] roomSizes) {
        return Stream.generate(() -> new Candidate("", roomSizes))
                .limit(quantity)
                .collect(Collectors.toList());
    }

}
