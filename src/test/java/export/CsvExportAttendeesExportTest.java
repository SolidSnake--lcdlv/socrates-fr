package export;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CsvExportAttendeesExportTest {

    private String filePath;

    @Before
    public void setUp() throws Exception {
        filePath = "src/test/resources/export_socrates_attendee.csv";

        try {
            Files.delete(Paths.get(filePath));
        } catch (Exception e) {
        }
    }

    @Test
    public void should_not_generate_csv_file_when_no_attendee_into_drawresult() throws Exception {

        DrawResultExport drawExportResult = new CsvDrawResultExport(filePath);

        ExportAttendees exportAttendees = new ExportAttendees();

        drawExportResult.export(exportAttendees);

        assertThat(new File(filePath).exists()).isFalse();
    }

    @Test
    public void should_generate_csv_file_when_at_least_one_attendee_into_drawresult() throws Exception {

        DrawResultExport drawExportResult = new CsvDrawResultExport(filePath);

        ExportAttendees exportAttendees = new ExportAttendees();
        exportAttendees.addAttendee(new ExportAttendee("test@test.fr", "single"));

        drawExportResult.export(exportAttendees);

        assertThat(new File(filePath).exists()).isTrue();
    }

    @Test
    public void should_generate_csv_file_with_first_line_and_this_line_is_the_header() throws Exception {

        DrawResultExport drawExportResult = new CsvDrawResultExport(filePath);

        ExportAttendees exportAttendees = new ExportAttendees();
        exportAttendees.addAttendee(new ExportAttendee("test@test.fr", "single"));

        drawExportResult.export(exportAttendees);

        String expectedFirstLine = Files.readAllLines(Paths.get(filePath)).get(0);

        assertThat(expectedFirstLine).isEqualTo("EMAIL, ROOM_TYPE");
    }

    @Test
    public void file_should_have_a_second_line_with_attendee_informations() throws Exception {

        DrawResultExport drawExportResult = new CsvDrawResultExport(filePath);

        ExportAttendees exportAttendees = new ExportAttendees();
        exportAttendees.addAttendee(new ExportAttendee("test@test.fr", "single"));

        drawExportResult.export(exportAttendees);

        String expectedSecondLine = Files.readAllLines(Paths.get(filePath)).get(1);

        assertThat(expectedSecondLine).isEqualTo("test@test.fr, single");
    }

    @Test
    public void file_should_have_five_lines_with_attendee_informations() throws Exception {

        DrawResultExport drawExportResult = new CsvDrawResultExport(filePath);

        ExportAttendees exportAttendees = new ExportAttendees();
        exportAttendees.addAttendee(new ExportAttendee("test@test.fr", "single"));
        exportAttendees.addAttendee(new ExportAttendee("test1@test.fr", "double"));
        exportAttendees.addAttendee(new ExportAttendee("test2@test.fr", "triple"));
        exportAttendees.addAttendee(new ExportAttendee("test3@test.fr", "single"));
        exportAttendees.addAttendee(new ExportAttendee("test4@test.fr", "double"));

        drawExportResult.export(exportAttendees);

        List<String> allLines = Files.readAllLines(Paths.get(filePath));

        assertThat(allLines.size()).isEqualTo(6);
    }
}