package notification;

public class GeneratorSpy implements Generator {

    int numberOfTimesGenerateHasBeenCalled = 0;

    @Override
    public Email generate(Destinary destinary) {
        numberOfTimesGenerateHasBeenCalled++;
        return new Email(destinary, new EmailTemplate("", "", ""));
    }

    public int getNumberOfTimesGenerateHasBeenCalled() {
        return numberOfTimesGenerateHasBeenCalled;
    }
}
