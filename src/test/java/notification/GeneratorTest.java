package notification;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GeneratorTest {

    @Test
    public void generate_something() throws Exception {

        EmailGenerator generator = new EmailSuccessGenerator();
        Destinary destinary = new Destinary("toto@lacombe.fr");

        assertThat(generator.generate(destinary)).isNotNull();
    }

    @Test
    public void output_address_should_be_the_same_as_input_address() throws Exception {
        EmailGenerator generator = new EmailSuccessGenerator();
        String addressMail = "toto@lacombe.fr";

        Destinary destinary = new Destinary(addressMail);

        Email email = generator.generate(destinary);
        assertThat(email.getTo()).isEqualTo(addressMail);
    }

    @Test
    public void successGenerator_generate_success_email() throws Exception {
        EmailGenerator generator = new EmailSuccessGenerator();
        String addressMail = "toto@lacombe.fr";

        Destinary destinary = new Destinary(addressMail);

        Email email = generator.generate(destinary);
        assertThat(email.getBody()).contains("You win");
    }

    @Test
    public void failureGenerator_generate_failure_email() throws Exception {
        EmailGenerator generator = new EmailFailureGenerator();
        String addressMail = "tata@lacombe.fr";

        Destinary destinary = new Destinary(addressMail);

        Email email = generator.generate(destinary);
        assertThat(email.getBody()).contains("You lose");
    }

}

