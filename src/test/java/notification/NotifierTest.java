package notification;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class NotifierTest {

    @Test
    public void notifier_orchestrates_dependencies_once() throws Exception {

        GeneratorSpy generator = new GeneratorSpy();
        SenderSpy sender = new SenderSpy();

        Notifier notifier = new Notifier(generator, sender);
        notifier.notify(new Destinary("test@test.fr"));

        Assertions.assertThat(generator.getNumberOfTimesGenerateHasBeenCalled()).isEqualTo(1);
        Assertions.assertThat(sender.getNumberOfTimesSendHasBeenCalled()).isEqualTo(1);
    }
}
