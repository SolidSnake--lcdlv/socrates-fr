package notification;

public class SenderSpy implements Sender {
    int numberOfTimesSendHasBeenCalled = 0;

    @Override
    public void sendEmail(Object toSend) {
        numberOfTimesSendHasBeenCalled ++;
    }

    public int getNumberOfTimesSendHasBeenCalled(){
        return numberOfTimesSendHasBeenCalled;
    }
}
