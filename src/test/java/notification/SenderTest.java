package notification;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SenderTest {

    @Test
    public void one_email_sent() throws Exception {

        GeneratorSpy generator = new GeneratorSpy();
        SenderSpy emailSenderMock = new SenderSpy();

        Notifier notifier = new Notifier(generator, emailSenderMock);
        notifier.notify(new Destinary("test@test.fr"));

        assertThat(emailSenderMock.getNumberOfTimesSendHasBeenCalled()).isEqualTo(1);
    }

}
