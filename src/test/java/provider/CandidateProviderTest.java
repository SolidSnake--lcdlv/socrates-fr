package provider;

import draw.Candidate;
import draw.Candidates;
import draw.RoomSize;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static draw.RoomSize.*;
import static org.assertj.core.api.Assertions.assertThat;

public class CandidateProviderTest {

    public static final String SRC_TEST_RESOURCES_CANDIDATE_DATA_CSV = "src/test/resources/candidate-data.csv";

    private ChoicesFilter choicesFilter;

    @Before
    public void setUp() throws Exception {
        choicesFilter = new ChoicesFilter();
    }

    @Test
    public void file_header_is_skipped() throws Exception {
        String candidatesFilePath = "src/test/resources/candidate-with-just-header.csv";

        CandidateProvider provider = new CsvCandidateProvider(candidatesFilePath, choicesFilter);

        assertThat(provider.getCandidates().getCandidates()).isEmpty();
    }

    @Test
    public void read_all_lines_of_file() throws Exception {
        CandidateProvider provider = new CsvCandidateProvider(SRC_TEST_RESOURCES_CANDIDATE_DATA_CSV, choicesFilter);

        assertThat(provider.getCandidates().getCandidates()).isNotEmpty();
    }

    @Test
    public void parse_candidate_from_line() throws Exception {
        CsvCandidateProvider provider = new CsvCandidateProvider(SRC_TEST_RESOURCES_CANDIDATE_DATA_CSV, choicesFilter);

        Candidates candidatesFromLines = provider.getCandidates();

        assertThat(candidatesFromLines.get(0)).isEqualTo(new Candidate("houssam.fakih@lcdlv.fr", SINGLE));
    }

    @Test
    public void no_accomodation_field() throws Exception {
        CsvCandidateProvider provider = new CsvCandidateProvider(SRC_TEST_RESOURCES_CANDIDATE_DATA_CSV, choicesFilter);

        Candidates candidatesFromLines = provider.getCandidates();

        assertThat(candidatesFromLines.get(7))
                .isEqualTo(new Candidate("mos.koziel@lcdlv.fr", TRIPLE, DOUBLE, NO_ACCOMODATION, SINGLE));
    }

    @Test
    public void only_two_choices() throws Exception {
        CsvCandidateProvider provider = new CsvCandidateProvider(SRC_TEST_RESOURCES_CANDIDATE_DATA_CSV, choicesFilter);

        Candidates candidatesFromLines = provider.getCandidates();

        assertThat(candidatesFromLines.get(8))
                .isEqualTo(new Candidate("patrick.giry@lcdlv.fr", DOUBLE, TRIPLE));
    }

    @Test
    public void candidate_validator_feature() throws Exception {

        String candidatesFilePath = "src/test/resources/candidates-acceptance-fourth-choice-not-unique.csv";

        Candidates expectedCandidates = new Candidates(Arrays.asList(
                new Candidate("1stchoice@lcdlv.fr", SINGLE),
                new Candidate("2ndchoice@lcdlv.fr", DOUBLE, SINGLE, TRIPLE),
                new Candidate("3rdchoice@lcdlv.fr", TRIPLE, DOUBLE)
        ));

        Candidates candidates = new CsvCandidateProvider(candidatesFilePath, choicesFilter).getCandidates();

        assertThat(candidates).isEqualTo(expectedCandidates);
    }

    @Test
    public void no_duplicate_candidate() throws Exception {

        String candidatesFilePath = "src/test/resources/candidate-duplication.csv";

        Candidates expectedCandidates = new Candidates(Arrays.asList(
                new Candidate("houssam.fakih@lcdlv.fr", SINGLE)
        ));

        Candidates candidates = new CsvCandidateProvider(candidatesFilePath, choicesFilter).getCandidates();

        assertThat(candidates).isEqualTo(expectedCandidates);
    }


    @Test
    public void filter_room_size_making_same_choices() throws Exception {

        Candidate candidate =
                new Candidate("1stchoice@lcdlv.fr", choicesFilter.removeDuplicateRoomSize(new RoomSize[]{DOUBLE, DOUBLE, DOUBLE, DOUBLE}));

        Candidate expectedCandidate = new Candidate("1stchoice@lcdlv.fr", DOUBLE);
        assertThat(candidate).isEqualTo(expectedCandidate);
    }

    @Test
    public void filter_room_size_and_keep_choice_order() throws Exception {

        Candidate candidate = new Candidate("1stchoice@lcdlv.fr", choicesFilter.removeDuplicateRoomSize(new RoomSize[]{DOUBLE, DOUBLE, TRIPLE, DOUBLE}));

        Candidate expectedCandidate = new Candidate("1stchoice@lcdlv.fr", DOUBLE, TRIPLE);

        assertThat(candidate).isEqualTo(expectedCandidate);
    }
}