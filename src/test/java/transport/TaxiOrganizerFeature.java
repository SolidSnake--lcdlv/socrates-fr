package transport;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.runners.MockitoJUnitRunner;
import transport.domain.TaxiOrganizer;
import transport.domain.passenger.PassengerProvider;
import transport.domain.taxi.TaxiFleetPrinter;
import transport.infrastructure.CSVPassengerProvider;
import transport.infrastructure.ConsoleTaxiFleetPrinter;

import java.io.PrintStream;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TaxiOrganizerFeature {

    private final String header = "Taxi Type | Date | Passenger #1 | Passenger #2 | Passenger #3 | Passenger #4 | Passenger #5 | Passenger #6";
    private final PrintStream out = mock(PrintStream.class);
    private TaxiFleetPrinter consolePrinter;

    @Before
    public void setUp() throws Exception {
        System.setOut(out);
        consolePrinter = new ConsoleTaxiFleetPrinter(System.out::println);
    }

    @Test
    public void organizer_should_assign_passengers_to_taxis() throws Exception {
        ArgumentCaptor<String> arguments = ArgumentCaptor.forClass(String.class);
        PassengerProvider passengerProvider = new CSVPassengerProvider("src/test/resources/passengers-data.csv");

        TaxiOrganizer taxiOrganizer = new TaxiOrganizer(passengerProvider, consolePrinter);

        taxiOrganizer.organize();

        verify(out, times(3)).println(arguments.capture());

        List<String> values = arguments.getAllValues();
        assertThat(values.get(0)).isEqualTo(header);

        assertThat(values.get(1))
                .startsWith("4 seats | 11:00 Thursday")
                .contains("Houssam Fakih", "Mickael Metesreau", "Boris Gonnot", "Anthyme Caillard");
        assertThat(values.get(2))
                .startsWith("4 seats | 12:54 Thursday")
                .contains("Jean Bisutti");
    }

    @Test
    public void organizer_should_assign_passengers_to_3_taxis() throws Exception {
        ArgumentCaptor<String> arguments = ArgumentCaptor.forClass(String.class);
        PassengerProvider passengerProvider = new CSVPassengerProvider("src/test/resources/passengers-data-10-at-differrent-time.csv");

        TaxiOrganizer taxiOrganizer = new TaxiOrganizer(passengerProvider, consolePrinter);

        taxiOrganizer.organize();

        verify(out, times(4)).println(arguments.capture());

        List<String> values = arguments.getAllValues();
        assertThat(values.get(0)).isEqualTo(header);

        assertThat(values.get(1))
                .startsWith("4 seats | 11:00 Thursday")
                .contains("Houssam Fakih", "Mickael Metesreau", "Boris Gonnot", "Anthyme Caillard");

        assertThat(values.get(2))
                .startsWith("4 seats | 11:00 Thursday")
                .contains("Kristy E. Robinson", "Michelle D. McClinton", "Peter K. Draper");

        assertThat(values.get(3))
                .startsWith("4 seats | 12:54 Thursday")
                .contains("David D. Ly", "Joanne K. Acosta", "Jean Bisutti");
    }

    @Test
    public void organizer_should_assign_passengers_to_a_taxi_of_4_and_a_taxi_of_6() throws Exception {
        ArgumentCaptor<String> arguments = ArgumentCaptor.forClass(String.class);
        PassengerProvider passengerProvider = new CSVPassengerProvider("src/test/resources/passengers-data-9-at-differrent-time.csv");

        TaxiOrganizer taxiOrganizer = new TaxiOrganizer(passengerProvider, consolePrinter);

        taxiOrganizer.organize();

        verify(out, times(3)).println(arguments.capture());

        List<String> values = arguments.getAllValues();
        assertThat(values.get(0)).isEqualTo(header);
        assertThat(values.get(1))
                .startsWith("6 seats | 11:00 Thursday")
                .contains("Houssam Fakih", "Mickael Metesreau", "Boris Gonnot", "Anthyme Caillard", "Kristy E. Robinson", "Michelle D. McClinton");
        assertThat(values.get(2))
                .startsWith("4 seats | 12:54 Thursday")
                .contains("David D. Ly", "Joanne K. Acosta", "Jean Bisutti");
    }
}
