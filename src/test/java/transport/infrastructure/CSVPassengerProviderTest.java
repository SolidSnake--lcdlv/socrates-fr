package transport.infrastructure;

import org.junit.Test;
import transport.domain.passenger.Passenger;
import transport.domain.passenger.PassengerProviderException;
import transport.domain.passenger.Passengers;
import transport.domain.taxi.Moment;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CSVPassengerProviderTest {

    @Test
    public void should_parse_a_line_and_return_passenger() throws Exception {
        String validLine = "Houssam Fakih @houssamfakih,TGV Paris-Orange/taxi,11:00 Thursday,Arrival Time at Orange then Taxi";

        Passenger expectedPassenger = new Passenger("Houssam Fakih", new Moment("11:00 Thursday"));

        assertThat(new CSVPassengerProvider("").parse(validLine))
                .isEqualTo(expectedPassenger);
    }

    @Test
    public void should_parse_a_different_line_and_return_passenger() throws Exception {
        String validLine = "Boris Gonnot @gonnot,TGV Paris-Orange/taxi,11:00 Thursday,Arrival Time at Orange then Taxi";

        Passenger expectedPassenger = new Passenger("Boris Gonnot", new Moment("11:00 Thursday"));

        assertThat(new CSVPassengerProvider("").parse(validLine))
                .isEqualTo(expectedPassenger);
    }

    @Test
    public void should_parse_a_different_moment_and_return_passenger() throws Exception {
        String validLine = "Boris Gonnot @gonnot,TGV Paris-Orange/taxi,12:00 Friday,Arrival Time at Orange then Taxi";

        Passenger expectedPassenger = new Passenger("Boris Gonnot", new Moment("12:00 Friday"));

        assertThat(new CSVPassengerProvider("").parse(validLine))
                .isEqualTo(expectedPassenger);
    }

    @Test
    public void should_return_two_passengers() throws Exception {
        String lineOne = "Houssam Fakih @houssamfakih,TGV Paris-Orange/taxi,11:00 Thursday,Arrival Time at Orange then Taxi";
        String lineTwo = "Boris Gonnot @gonnot,TGV Paris-Orange/taxi,12:00 Friday,Arrival Time at Orange then Taxi";

        Passenger expectedPassenger1 = new Passenger("Houssam Fakih", new Moment("11:00 Thursday"));
        Passenger expectedPassenger2 = new Passenger("Boris Gonnot", new Moment("12:00 Friday"));
        List<Passenger> expectedPassengers = Arrays.asList(expectedPassenger1, expectedPassenger2);

        assertThat(new CSVPassengerProvider("").parseAllLines(Arrays.asList(lineOne, lineTwo)))
                .isEqualTo(new Passengers(expectedPassengers));
    }

    @Test
    public void file_header_is_skip() throws Exception {

        String path = "src/test/resources/passengers-data.csv";

        assertThat(new CSVPassengerProvider(path).gatherPassengers().getPassengers()).hasSize(5);
    }

    @Test
    public void should_gather_passengers_from_file() {

        String path = "src/test/resources/passengers-data.csv";

        Passenger HoussamFakih = new Passenger("Houssam Fakih", new Moment("11:00 Thursday"));
        Passenger MickaelMetesreau = new Passenger("Mickael Metesreau", new Moment("11:00 Thursday"));
        Passenger BorisGonnot = new Passenger("Boris Gonnot", new Moment("11:00 Thursday"));
        Passenger AnthymeCaillard = new Passenger("Anthyme Caillard", new Moment("11:00 Thursday"));
        Passenger JeanBisutti = new Passenger("Jean Bisutti", new Moment("12:54 Thursday"));

        List<Passenger> expectedPassengers = Arrays.asList(HoussamFakih, MickaelMetesreau, BorisGonnot, AnthymeCaillard, JeanBisutti);

        assertThat(new CSVPassengerProvider(path).gatherPassengers())
                .isEqualTo(new Passengers(expectedPassengers));
    }

    @Test(expected = PassengerProviderException.class)
    public void should_return_csv_parse_exception() throws Exception {

        String path = "";

        new CSVPassengerProvider(path).gatherPassengers();
    }
}