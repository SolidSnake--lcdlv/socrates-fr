package transport.infrastructure;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.runners.MockitoJUnitRunner;
import transport.domain.passenger.Passenger;
import transport.domain.passenger.Passengers;
import transport.domain.taxi.*;

import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.atLeastOnce;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleTaxiFleetPrinterTest {
    private final PrintStream out = mock(PrintStream.class);
    private TaxiFleetPrinter taxiFleetPrinter;

    @Before
    public void setUp() throws Exception {
        System.setOut(out);
        taxiFleetPrinter = ConsoleTaxiFleetPrinter.Build(System.out::println);
    }

    @Test
    public void print_correct_header() throws Exception {
        taxiFleetPrinter.printAssignments(new TaxiFleet());
        verify(out).println("Taxi Type | Date | Passenger #1 | Passenger #2 | Passenger #3 | Passenger #4 | Passenger #5 | Passenger #6");
    }

    @Test
    public void print_correct_passenger() throws Exception {
        ArgumentCaptor<String> arguments = ArgumentCaptor.forClass(String.class);

        Moment friday = new Moment("11:00 Friday");
        Passenger houssam = new Passenger("Houssam F.", friday);
        TaxiFleet taxiFleet = new TaxiFleet();
        Taxi taxi = new Taxi(new Shipment(friday, new Passengers().addPassenger(houssam)), new Capacity(4));
        taxiFleet.addTaxi(taxi);

        taxiFleetPrinter.printAssignments(taxiFleet);

        verify(out, atLeastOnce()).println(arguments.capture());

        assertThat(arguments.getAllValues())
                .contains("Taxi Type | Date | Passenger #1 | Passenger #2 | Passenger #3 | Passenger #4 | Passenger #5 | Passenger #6",
                        "4 seats | 11:00 Friday | Houssam F."
                );
    }
}