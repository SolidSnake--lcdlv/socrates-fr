package transport.passenger;

import org.junit.Test;
import transport.domain.passenger.Passenger;
import transport.domain.passenger.Passengers;
import transport.domain.taxi.Moment;
import transport.domain.taxi.Shipment;
import transport.domain.taxi.Shipments;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class PassengersTest {

    @Test
    public void should_return_all_passengers_at_same_arrival_moment() throws Exception {
        Passengers passengers = generatePassengers(new Moment("12:00 Friday"), 7);
        passengers = generatePassengers(new Moment("11:00 Thursday"), 5, passengers);

        Moment thursday11am = new Moment("11:00 Thursday");
        Moment friday12pm = new Moment("12:00 Friday");
        Passengers passengers12Friday = generatePassengers(friday12pm, 7);
        Passengers passengers11Thursday = generatePassengers(thursday11am, 5);

        Shipments shipments = Shipments.optimize(Arrays.asList(new Shipment(thursday11am, passengers11Thursday), new Shipment(friday12pm, passengers12Friday)));

        Shipments actual = passengers.groupByMoment();

        assertThat(actual).isEqualTo(shipments);
    }

    private Passengers generatePassengers(Moment moment, int quantity) {
        return generatePassengers(moment, quantity, new Passengers());
    }

    private Passengers generatePassengers(Moment moment, int quantity, Passengers initialPassengers) {
        Passengers generatedPassengers = new Passengers(initialPassengers.getPassengers());

        for (int i = 0; i < quantity; i++) {
            generatedPassengers.addPassenger(new Passenger("Passenger #" + i, moment));
        }

        return generatedPassengers;
    }
}