package transport.taxi;

import org.junit.Test;
import transport.domain.passenger.Passenger;
import transport.domain.passenger.Passengers;
import transport.domain.taxi.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static transport.domain.taxi.TaxiFleet.divideShipment;

public class TaxiFleetDivideShipmentTest {

    private final List<Passenger> passengers = generatePassengers(20);

    @Test
    public void divide_one_passenger_to_one_taxi_with_four_place() throws Exception {
        Shipments shipments = given_a_shipment_with_one_passenger_arriving_at_11();

        TaxiFleet actual = divideShipment(shipments);

        assertThat(actual).isEqualTo(taxifleet_with_one_taxi_one_passenger());
    }

    @Test
    public void divide_eight_passengers_to_two_taxi_with_four_place() throws Exception {
        Shipments shipments = given_a_shipment_with_eight_passenger_arriving_at_11();

        TaxiFleet actual = divideShipment(shipments);

        assertThat(actual).isEqualTo(taxifleet_with_two_taxis_of_four_passengers());
    }

    @Test
    public void divide_six_passengers_to_on_taxi_with_six_place() throws Exception {
        Shipments shipment = given_a_shipment_with_six_passenger_arriving_at_11();

        TaxiFleet actual = divideShipment(shipment);

        assertThat(actual).isEqualTo(taxifleet_with_one_taxi_of_six_passengers());
    }

    @Test
    public void divide_twelve_passengers_to_three_taxis() throws Exception {
        Shipments shipments = given_two_shipments_with_twelve_passengers();

        TaxiFleet actual = divideShipment(shipments);

        assertThat(actual).isEqualTo(taxifleet_with_three_taxis_of_six_passengers_and_four_and_four());
    }

    @Test
    public void divide_fifteen_passengers_to_four_taxis() throws Exception {
        Shipments shipments = given_a_shipment_with_fifteen_passenger_arriving_at_11();
        TaxiFleet actual = divideShipment(shipments);

        assertThat(actual).isEqualTo(taxifleet_with_four_taxis_of_six_passengers_and_three_times_four());
    }

    private Shipments given_a_shipment_with_fifteen_passenger_arriving_at_11() {
        return getShipments(0, 15);
    }

    private Shipments getShipments(int from, int to) {
        List<Shipment> shipments = new ArrayList<>();
        shipments.add(getShipmentWithPassengers(from, to));
        return Shipments.optimize(shipments);
    }

    private TaxiFleet taxifleet_with_four_taxis_of_six_passengers_and_three_times_four() {
        TaxiFleet expectedTaxiFleet = new TaxiFleet();
        Taxi taxi = new Taxi(getShipmentWithPassengers(0, 6), new Capacity(6));
        Taxi taxi2 = new Taxi(getShipmentWithPassengers(6, 9), new Capacity(4));
        Taxi taxi3 = new Taxi(getShipmentWithPassengers(9, 12), new Capacity(4));
        Taxi taxi4 = new Taxi(getShipmentWithPassengers(12, 15), new Capacity(4));
        expectedTaxiFleet.addTaxi(taxi);
        expectedTaxiFleet.addTaxi(taxi2);
        expectedTaxiFleet.addTaxi(taxi3);
        expectedTaxiFleet.addTaxi(taxi4);
        return expectedTaxiFleet;
    }

    private TaxiFleet taxifleet_with_three_taxis_of_six_passengers_and_four_and_four() {
        TaxiFleet expectedTaxiFleet = new TaxiFleet();
        Taxi taxi = new Taxi(getShipmentWithPassengers(0, 6), new Capacity(6));
        Taxi taxi2 = new Taxi(getShipmentWithPassengers(6, 9), new Capacity(4));
        Taxi taxi3 = new Taxi(getShipmentWithPassengers(9, 12), new Capacity(4));
        expectedTaxiFleet.addTaxi(taxi);
        expectedTaxiFleet.addTaxi(taxi2);
        expectedTaxiFleet.addTaxi(taxi3);
        return expectedTaxiFleet;
    }

    private Shipments given_two_shipments_with_twelve_passengers() {
        return getShipments(0, 12);
    }

    private Shipments given_a_shipment_with_six_passenger_arriving_at_11() {
        return getShipments(0, 6);
    }

    private TaxiFleet taxifleet_with_one_taxi_of_six_passengers() {
        TaxiFleet expectedTaxiFleet = new TaxiFleet();
        Taxi taxi = new Taxi(getShipmentWithPassengers(0, 6), new Capacity(6));
        expectedTaxiFleet.addTaxi(taxi);
        return expectedTaxiFleet;
    }

    private Shipments given_a_shipment_with_one_passenger_arriving_at_11() {
        Shipment shipment = newShipmentWithOnePassenger();
        List<Shipment> shipments = new ArrayList<>();
        shipments.add(shipment);
        return Shipments.optimize(shipments);
    }

    private Shipments given_a_shipment_with_eight_passenger_arriving_at_11() {
        return getShipments(0, 8);
    }


    private Shipment getShipmentWithPassengers(int from, int to) {
        Passengers passengers = new Passengers();
        this.passengers.subList(from, to)
                .forEach(passengers::addPassenger);
        return new Shipment(new Moment("11:00 Friday"), passengers);
    }

    private List<Passenger> generatePassengers(int numberOfPassengers) {
        List<Passenger> passengers = new ArrayList<>();
        IntStream.range(0, numberOfPassengers)
                .forEach(x -> passengers.add(generatePassenger(x)));
        return passengers;
    }

    private Passenger generatePassenger(int index) {
        return new Passenger("Passenger #" + index, new Moment("11:00 Friday"));
    }

    private TaxiFleet taxifleet_with_one_taxi_one_passenger() {
        TaxiFleet expectedTaxiFleet = new TaxiFleet();
        Taxi taxi = new Taxi(newShipmentWithOnePassenger(), new Capacity(4));
        expectedTaxiFleet.addTaxi(taxi);
        return expectedTaxiFleet;
    }

    private TaxiFleet taxifleet_with_two_taxis_of_four_passengers() {
        TaxiFleet expectedTaxiFleet = new TaxiFleet();
        Taxi taxi = new Taxi(getShipmentWithPassengers(0, 4), new Capacity(4));
        Taxi taxi2 = new Taxi(getShipmentWithPassengers(4, 8), new Capacity(4));
        expectedTaxiFleet.addTaxi(taxi);
        expectedTaxiFleet.addTaxi(taxi2);
        return expectedTaxiFleet;
    }

    private Shipment newShipmentWithOnePassenger() {
        return getShipmentWithPassengers(0, 1);
    }
}